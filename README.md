# tools

This project is to create/manage tools that can be used by all my other projects.

i.e. Instead of having to recreate a file reader/writer function

Top level folder is the language, next level is tool name (folder) with tools supporting files/folders

EXAMPLE:

1.  Python
    1.  FileManager
        1.  DOCS
        *  class FileMan - main methods/functions
        *  class reader - called by FileMan to read files
        *  class writer - called by FileMan to write files
        *  class fileConnector - used to connect to other location
    2.  SSHmanager
        1.  DOCS
        *  class SSHMan - main methods/functions
        *  class ....................
        *  .................

file system:

*  readme.md
*  license
*  Python (FOLDER)
    * FileManager (FOLDER)
        *  DOCS (FOLDER)        
        *  __init__.py
        *  FileMan.py
        *  reader.py
        *  writer.py
        *  fileConnector.py
    * SSHManager (FOLDER)
        *  DOCS (FOLDER)     
        *  __init__.py
        *  SSHMan.py
        *  ..............
*  Java (FOLDER)
    *  Project (FOLDER)
        *  DOCS (FOLDER)     
        *  project files/folders